const qs = require('qs');

export const Constants = {
    // API_PATH: process.env.api[process.env.NODE_ENV],
    // API_PATH: 'https://adonisjsbackend.herokuapp.com',
    API_PATH: 'http://192.168.0.174:3333',
    APP_PATH: 'https://localhost:3000',
}
export const Api = {
    baseurl: (headPoint: string, params: object | string) => `${Constants.API_PATH}/${headPoint}${params ? `?${qs.stringify(params)}` : ""}`,
    appurl: (headPoint: string, params: object) => `${Constants.APP_PATH}/${headPoint}${params ? `?${qs.stringify(params)}` : ""}`,
    imageUrl: (url: string) => `${Constants.API_PATH}${url}`,
    login: process.env.NODE_ENV === "development" ? "purellarohith@gmail.com" : "",
    password: process.env.NODE_ENV === "development" ? "asdfghjkl" : "",
};