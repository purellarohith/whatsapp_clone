import React, { useState } from 'react';
import moment from 'moment-timezone';
import { NextRouter, useRouter } from 'next/router';
import { Api } from '../constants/gobal_constants';
const qs = require('qs');

const useServer = () => {
    const router: NextRouter = useRouter()

    const [Loading, setLoading] = useState<boolean>(false)
    const callApi = async (headPoint: string, bodyData: object, show: boolean, method: string, isForm: boolean, token: string) => {
        setLoading(true);

        try {
            let response = await fetch(Api.baseurl(headPoint, (method !== 'get' ? '' : bodyData)), {
                method: method,
                ...(
                    method !== 'get' &&
                    {
                        body: isForm ? bodyData : qs.stringify(bodyData, { arrayFormat: 'brackets' })
                    }
                ),
                headers: {
                    ...(!isForm && { 'Content-Type': 'application/x-www-form-urlencoded' }),
                    'Authorization': token ? `Bearer ${token}` : '',
                    'x-accept-timezone': moment.tz.guess()
                },
            });

            response = await response.json();
            setLoading(false);
            return response;
        } catch (err) {
            setLoading(false);
            // show &&  {do something};
            return { error: 0, data: null };
        }
    }

    return [
        callApi,
        Loading,
        setLoading,
    ];
}






export default useServer