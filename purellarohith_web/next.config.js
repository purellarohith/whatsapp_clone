/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    swcMinify: true,
    eslint: {
        ignoreDuringBuilds: true,
    },
    images: {
        domains: ['https://adonisjsbackend.herokuapp.com'],
    },
    env: {
        api: {
            production: 'https://adonisjsbackend.herokuapp.com',
            development: 'https://adonisjsbackend.herokuapp.com'
        }
    }

}

module.exports = nextConfig
