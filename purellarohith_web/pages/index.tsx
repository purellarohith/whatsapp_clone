import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import { useEffect, useState } from 'react'
import useServer from '../hooks/useServer'
import styles from '../styles/Home.module.scss'

const Home: NextPage = () => {

    const [callApi, Loading, setLoading]: any = useServer()
    const [name, setName] = useState('')
    const [serverName, setServerName] = useState('Admin')

    useEffect(() => {
        init()
    }, [])



    const init = async () => {
        let response = await callApi('hello', null, false, 'get', false, '')
    }


    const updateServer = async () => {
        if (name !== serverName && name.trim() !== "") {
            let response = await callApi('user', { name: name }, false, 'post', false, '')
            setServerName(response.data.name)
            setName('')
        }
    }


    return (
        <div className={styles.container}>
            <Head>
                <title>Home Page</title>
                <meta name="description" content="Home page of purellarohith.com" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <div className={styles.main_wrapper}>
                <h1>{`Welcome ${name}`}</h1>
                <input className={styles.input_wrapper} value={name} onChange={(e) => setName(e.target.value)} />
                <button onClick={() => updateServer()}>update</button>
            </div>

        </div>
    )
}

export default Home
