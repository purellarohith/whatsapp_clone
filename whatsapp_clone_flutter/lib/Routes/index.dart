import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp_clone_flutter/Whatsapp/main.dart';

class CustomRoute extends StatefulWidget {
  const CustomRoute({Key? key}) : super(key: key);

  @override
  State<CustomRoute> createState() => CustomRouteState();
}

class CustomRouteState extends State<CustomRoute> {
  final int _appsState = 0;

  static final List<Widget> _screens = <Widget>[WhatsappHome()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child: _screens[_appsState]),
    );
  }
}
