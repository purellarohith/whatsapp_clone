import 'package:flutter/material.dart';

Widget profileIcons({required double size, required IconData iconData}) {
  return Container(
      width: size,
      height: size,
      decoration: const BoxDecoration(
        shape: BoxShape.circle,
        gradient: LinearGradient(
            colors: [Colors.blueAccent, Colors.blueGrey],
            begin: Alignment(0.1, 0.2),
            end: Alignment(1, 1)),
      ),
      child: Icon(
        iconData,
        color: Colors.white,
        size: 32,
      ));
}

Widget profileStoryIcons({required double size, required String imageData}) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 8.0),
    child: Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
                image: NetworkImage(imageData), fit: BoxFit.cover))),
  );
}
