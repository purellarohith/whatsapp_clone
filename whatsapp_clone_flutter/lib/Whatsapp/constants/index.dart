import 'dart:ui';

class DoubleShades {
  final Color lightShade;
  final Color darkShade;

  DoubleShades(this.darkShade, this.lightShade);
}

const Color backgroundColor = Color(0xFFFFFFFF);
const Color greenColor = Color(0xFF5CE27F);
const Color yellowColor = Color(0xFFFFE12D);
const Color redColor = Color(0xFFE25C5C);
DoubleShades blackColor = DoubleShades(Color(0xFF313131), Color(0xFF121212));
DoubleShades grayColor = DoubleShades(Color(0xFFB18181), Color(0xFFEFEFEF));

String dummyImage =
    'https://images.unsplash.com/photo-1487412720507-e7ab37603c6f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1471&q=80';
