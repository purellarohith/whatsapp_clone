import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatsapp_clone_flutter/Whatsapp/components/profileicon/index.dart';
import 'package:whatsapp_clone_flutter/Whatsapp/constants/index.dart';

class WhatsappHome extends StatefulWidget {
  WhatsappHome({Key? key}) : super(key: key);

  @override
  State<WhatsappHome> createState() => _WhatsappHomeState();
}

class _WhatsappHomeState extends State<WhatsappHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.only(left: 16.4, right: 16.4),
        child: Column(
          children: [
            const SizedBox(height: 26),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                Text(
                  "Chat",
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700),
                ),
                Icon(Icons.group_add, color: greenColor, size: 32)
              ],
            ),
            const SizedBox(height: 26),
            const Divider(
              thickness: 1,
            ),
            Row(
              children: [
                profileIcons(iconData: Icons.add, size: 60),
                Container(
                  height: 100,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    children: List.generate(
                      5,
                      (index) =>
                          profileStoryIcons(imageData: dummyImage, size: 60),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      )),
    );
  }
}
